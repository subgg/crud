<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BookModelController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('welcome');
});

Route::resource('Book', BookModelController::class);



// Route::get('/books/{$id}', [
//     'as'	=>	'books-get',
//     'uses'	=>	'BookModelController@getBookList'
// ]);


