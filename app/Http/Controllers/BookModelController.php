<?php

namespace App\Http\Controllers;

use App\Models\BookModel;
use Illuminate\Http\Request;

class BookModelController extends Controller
{
    // public function postAdminGroupCreate(){
	// 	$input = request()->all();

	// 	$validator = \Validator::make($input['data'], (new BookModel)->getRule());

    //     if($validator->fails()) {
    //         \Session::flash('friendly-error-msg', 'There are some validation errors');
    //         return redirect()->back()
    //                         ->withInput()
    //                         ->withErrors($validator);
    //     }

	// 	BookModel::create([
	// 		'name' => $input['data']['name']
	// 	]);

	// 	session()->flash('success-msg', 'User group successfully created');

	// 	return redirect()->back();
	// }





    // public function getBookList($id){
    //     $groups = BookModel::where('id', $id)->with('Book')->get();
	// 	// return $groups;

	// 	return view($this->view.'list-member')
	// 		->with('id', $id);

    // }

    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $book = BookModel::latest()->paginate(5);

        return view('Book.index', compact('book'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        return view('Book.create');
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'publisher' => 'required',
            'author' => 'required',
            'stock' => 'required'
        ]);

        BookModel::create($request->all());

        return redirect()->route('Book.index')
            ->with('success', 'Project created successfully.');
    }


    /**
     * Display the specified resource.
     *
     */
    public function show(BookModel $book)
    {
        return view('Book.show', compact('book'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     */
    public function edit(BookModel $book)
    {
        return view('Book.edit', compact('book'));
    }


    /**
     * Update the specified resource in storage.
     *
     */
    public function update(Request $request, BookModel $book)
    {
        $request->validate([
            'name' => 'required',
            'publisher' => 'required',
            'author' => 'required',
            'stock' => 'required'
        ]);
        $book->update($request->all());

        return redirect()->route('Book.index')
            ->with('success', 'Project updated successfully');
    }


    /**
     * Remove the specified resource from storage.
     *
     */
    public function destroy(BookModel $book)
    {
        $book->delete();

        return redirect()->route('Book.index')
            ->with('success', 'Project deleted successfully');
    }

}
