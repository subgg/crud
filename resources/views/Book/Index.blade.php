@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Laravel 8 CRUD </h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('Book.create') }}" title="Insert a book"> <i class="fas fa-plus-circle"></i>
                    </a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered table-responsive-lg">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Publisher</th>
            <th>Author</th>
            <th>Stock</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($book as $index => $d)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $d->name }}</td>
                <td>{{ $d->publisher }}</td>
                <td>{{ $d->author }}</td>
                <td>{{ $d->stock }}</td>
                <td>
                    <form action="{{ route('Book.destroy', $book->id) }}" method="POST">

                        <a href="{{ route('Book.show', $book->id) }}" title="show">
                            <i class="fas fa-eye text-success  fa-lg"></i>
                        </a>

                        <a href="{{ route('Book.edit', $book->id) }}">
                            <i class="fas fa-edit  fa-lg"></i>

                        </a>

                        @csrf
                        @method('DELETE')

                        <button type="submit" title="delete" style="border: none; background-color:transparent;">
                            <i class="fas fa-trash fa-lg text-danger"></i>

                        </button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>

    {!! $books->links() !!}

@endsection
